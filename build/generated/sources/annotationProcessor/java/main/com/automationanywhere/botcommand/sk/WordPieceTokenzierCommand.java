package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class WordPieceTokenzierCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(WordPieceTokenzierCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    WordPieceTokenzier command = new WordPieceTokenzier();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("text") && parameters.get("text") != null && parameters.get("text").get() != null) {
      convertedParameters.put("text", parameters.get("text").get());
      if(!(convertedParameters.get("text") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","text", "String", parameters.get("text").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("text") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","text"));
    }

    if(parameters.containsKey("vocabulary") && parameters.get("vocabulary") != null && parameters.get("vocabulary").get() != null) {
      convertedParameters.put("vocabulary", parameters.get("vocabulary").get());
      if(!(convertedParameters.get("vocabulary") instanceof List)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","vocabulary", "List", parameters.get("vocabulary").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("vocabulary") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","vocabulary"));
    }

    if(parameters.containsKey("fuzzy") && parameters.get("fuzzy") != null && parameters.get("fuzzy").get() != null) {
      convertedParameters.put("fuzzy", parameters.get("fuzzy").get());
      if(!(convertedParameters.get("fuzzy") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","fuzzy", "Boolean", parameters.get("fuzzy").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("numeric") && parameters.get("numeric") != null && parameters.get("numeric").get() != null) {
      convertedParameters.put("numeric", parameters.get("numeric").get());
      if(!(convertedParameters.get("numeric") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","numeric", "Boolean", parameters.get("numeric").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("confidence") && parameters.get("confidence") != null && parameters.get("confidence").get() != null) {
      convertedParameters.put("confidence", parameters.get("confidence").get());
      if(!(convertedParameters.get("confidence") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","confidence", "Number", parameters.get("confidence").get().getClass().getSimpleName()));
      }
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("text"),(List<Value>)convertedParameters.get("vocabulary"),(Boolean)convertedParameters.get("fuzzy"),(Boolean)convertedParameters.get("numeric"),(Number)convertedParameters.get("confidence")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
